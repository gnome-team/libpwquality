Source: libpwquality
Section: devel
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Iain Lane <laney@debian.org>,
           Jeremy Bícha <jbicha@ubuntu.com>,
           Laurent Bigonville <bigon@debian.org>,
           Michael Biebl <biebl@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-gnome,
               dh-sequence-python3,
               libcrack2-dev,
               libpam0g-dev,
               libpython3-dev,
               python3-dev:any,
               python3-setuptools
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/gnome-team/libpwquality
Vcs-Git: https://salsa.debian.org/gnome-team/libpwquality.git
Homepage: https://github.com/libpwquality/libpwquality

Package: libpwquality-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libpwquality1 (= ${binary:Version}), ${misc:Depends}
Description: Password quality checking and generation (development files)
 libpwquality's purpose is to provide common functions for password
 quality checking and also scoring them based on their apparent randomness. The
 library also provides a function for generating random passwords with good
 pronounceability.
 .
 This package contains the development files, and should be used for
 compilation.

Package: libpwquality1
Section: libs
Architecture: any
Multi-Arch: same
Depends: libpwquality-common, ${misc:Depends}, ${shlibs:Depends}
Description: library for password quality checking and generation
 libpwquality's purpose is to provide common functions for password
 quality checking and also scoring them based on their apparent randomness. The
 library also provides a function for generating random passwords with good
 pronounceability.
 .
 This package contains the shared library.

Package: libpwquality-common
Section: libs
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Breaks: libpwquality-tools (<< 1.4.1-2~)
Replaces: libpwquality-tools (<< 1.4.1-2~)
Description: library for password quality checking and generation (data files)
 libpwquality's purpose is to provide common functions for password
 quality checking and also scoring them based on their apparent randomness. The
 library also provides a function for generating random passwords with good
 pronounceability.
 .
 This package contains the configuration file and man page for libpwquality.

Package: libpwquality-tools
Section: admin
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: cracklib-runtime
Description: tools for password quality checking and generation
 libpwquality's purpose is to provide common functions for password
 quality checking and also scoring them based on their apparent randomness. The
 library also provides a function for generating random passwords with good
 pronounceability.
 .
 Tools for password quality checking and generation.

Package: libpam-pwquality
Section: admin
Architecture: any
Multi-Arch: same
Depends: libpam-runtime, ${misc:Depends}, ${shlibs:Depends}
Description: PAM module to check password strength
 libpwquality's purpose is to provide common functions for password
 quality checking and also scoring them based on their apparent randomness. The
 library also provides a function for generating random passwords with good
 pronounceability.
 .
 This module can be plugged into the password stack of a given service to
 provide some plug-in strength-checking for passwords. The code was originaly
 based on pam_cracklib module and the module is backwards compatible with its
 options.

Package: python3-pwquality
Architecture: any
Multi-Arch: same
Section: python
Depends: ${misc:Depends}, ${python3:Depends}, ${shlibs:Depends}
Description: Python bindings for libpwquality
 libpwquality's purpose is to provide common functions for password
 quality checking and also scoring them based on their apparent randomness. The
 library also provides a function for generating random passwords with good
 pronounceability.
 .
 This package contains the Python bindings.
